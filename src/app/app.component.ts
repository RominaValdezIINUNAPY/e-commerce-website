import {Component} from '@angular/core';
import {AuthService} from "./modules/auth";
import {InformacionService} from "./services/informacion.service";
import {ProductosService} from "./services/productos.service";
import {ColorPickerService} from 'angular4-color-picker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['app.component.css'],
  providers:[AuthService]
})
export class AppComponent {
  overlay = false;
  constructor(public _is: InformacionService,
              public _ps: ProductosService,
              private cpService: ColorPickerService) {
  }
}
