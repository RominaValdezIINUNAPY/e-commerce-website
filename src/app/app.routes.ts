
import { RouterModule, Routes } from '@angular/router';

import {RequireAuthGuard} from "./modules/auth";
import {FormularioComponent} from "./components/formulario/formulario.component";
import {PortafolioComponent} from "./components/portafolio/portafolio.component";
import {AboutComponent} from "./components/about/about.component";
import {ProductoComponent} from "./components/producto/producto.component";
import {SearchComponent} from "./components/search/search.component";
import {PopUpPortafolioComponent} from "./components/pop-up-portafolio/pop-up-portafolio.component";
import {ListadoComponent} from "./components/listado/listado.component";
import {CarritoComponent} from "./components/carrito/carrito.component";
import {GraficosComponent} from "./components/graficos/graficos.component";


const app_routes: Routes = [
  {path: 'listado',  component: ListadoComponent, canActivate: [RequireAuthGuard]  },
  {path: 'formulario',  component: FormularioComponent, canActivate: [RequireAuthGuard]  },
  {path: 'carrito',  component: CarritoComponent, canActivate: [RequireAuthGuard]  },
  {path: 'graficos',  component: GraficosComponent, canActivate: [RequireAuthGuard]  },
  { path: 'home', component: PortafolioComponent, canActivate: [RequireAuthGuard]  },
  { path: 'about', component: AboutComponent, canActivate: [RequireAuthGuard]  },
  { path: 'producto/:id', component: ProductoComponent, canActivate: [RequireAuthGuard]  },
  { path: 'producto-pop-up/:id', component: PopUpPortafolioComponent, canActivate: [RequireAuthGuard]  },
  { path: 'buscar/:termino', component: SearchComponent, canActivate: [RequireAuthGuard]  },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }

];

export const app_routing = RouterModule.forRoot(app_routes, { useHash:true });
