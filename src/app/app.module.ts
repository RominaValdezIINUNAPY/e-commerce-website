import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';

import { AppComponent } from './app.component';

import {AuthModule} from './modules/auth';
import {FirebaseModule} from './modules/firebase/firebase.module';
import {app_routing} from './app.routes';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {SearchComponent} from './components/search/search.component';
import {ProductoComponent} from './components/producto/producto.component';
import {PortafolioComponent} from './components/portafolio/portafolio.component';
import {AboutComponent} from './components/about/about.component';
import {HttpModule} from '@angular/http';
import {InformacionService} from './services/informacion.service';
import {ProductosService} from './services/productos.service';
import {FormularioComponent} from './components/formulario/formulario.component';
import { PopUpPortafolioComponent } from './components/pop-up-portafolio/pop-up-portafolio.component';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';

import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {
  MdTableModule, MdButtonModule, MdMenuModule, MdToolbarModule, MdIconModule,
  MdCardModule, MdDialogModule, MdFormFieldModule, MdSelectModule, MdOptionModule, MdInputModule,
  } from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UploadService} from './services/upload.service';
import {ColorPickerModule} from 'angular4-color-picker';
import { ListadoComponent } from './components/listado/listado.component';
import { CarritoComponent } from './components/carrito/carrito.component';
import {CarritoService} from './services/carrito.service';
import {VentasService} from './services/ventas.service';
import { GraficosComponent } from './components/graficos/graficos.component';
import { AngularEchartsModule } from 'ngx-echarts';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PortafolioComponent,
    AboutComponent,
    ProductoComponent,
    SearchComponent,
    FormularioComponent,
    PopUpPortafolioComponent,
    ListadoComponent,
    CarritoComponent,
    GraficosComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MdTableModule,
    MdButtonModule,
    MdMenuModule,
    MdToolbarModule,
    MdIconModule,
    MdCardModule,
    MdDialogModule,
    MdFormFieldModule,
    MdSelectModule,
    MdOptionModule,
    MdFormFieldModule,
    MdInputModule,
    BootstrapModalModule,
    BrowserModule,
    BrowserAnimationsModule,
    BootstrapModalModule.forRoot({container: document.body}),
    HttpModule,
    RouterModule.forRoot([], {useHash: false}),
    ColorPickerModule,
    AuthModule,
    FirebaseModule,
    AngularEchartsModule,
    app_routing
  ],
  entryComponents: [
    PopUpPortafolioComponent
  ],
  providers: [
    InformacionService,
    ProductosService,
    UploadService,
    CarritoService,
    VentasService
  ],
  bootstrap: [AppComponent],

})
export class AppModule { }
