import { Component } from '@angular/core';

import { InformacionService } from "../../services/informacion.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: [
    '../../../assets/css/aurora-pack.min.css',
    '../../../assets/css/aurora-theme-base.min.css',
    '../../../assets/css/urku.css'
  ]})
export class FooterComponent {

  anio:number = new Date().getFullYear();

  constructor( public _is:InformacionService ){ }

}
