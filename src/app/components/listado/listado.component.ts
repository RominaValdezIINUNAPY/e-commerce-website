
import { Component, OnInit } from '@angular/core';

import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import {Producto} from '../../models/producto';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  productos: FirebaseListObservable<Producto[]>;

  constructor(db: AngularFireDatabase) {
    this.productos = db.list('productos');
  }

  ngOnInit() {
  }
}



