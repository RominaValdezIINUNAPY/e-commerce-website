import {Component, OnInit} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import { NgForm } from '@angular/forms';
import {Upload} from '../../services/upload';
import {UploadService} from '../../services/upload.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-formulario',
  templateUrl: 'formulario.component.html',
  styleUrls: ['formulario.component.scss']
})


export class FormularioComponent implements OnInit {
  selectedValue: string;
  selectedFiles: FileList;
  currentUpload: Upload;
  color: string;



  tipo_productos = [
    {value: 'comestibles', viewValue: 'Comestibles'},
    {value: 'vestimenta', viewValue: 'Vestimenta'},
    {value: 'hogar', viewValue: 'Hogar'},
    {value: 'vehiculo', viewValue: 'Vehiculo'}
  ];


  ngOnInit() {
  }

  constructor(
    private angularFire: AngularFireDatabase,
    private upSvc: UploadService,
  ) { }

  form_submit(f: NgForm) {
    const files = this.selectedFiles;

    if (_.isEmpty(files)) return;

    if (files.length > 5) return alert('Solo puede subir hasta 5 fotos');

    const filesIndex = _.range(files.length);
    _.each(filesIndex, (idx) => {
      this.currentUpload = new Upload(files[idx]);
      this.upSvc.pushUpload(this.currentUpload, f.controls.codigo.value); }
    );


    this.angularFire.list('productos').push(
      {
        codigo: f.controls.codigo.value,
        nombre: f.controls.nombre.value,
        descripcion: f.controls.descripcion.value,
        precio: f.controls.precio.value,
        stock: f.controls.stock.value,
        color: this.color,
        dimensiones: f.controls.dimensiones.value,
        peso: f.controls.peso.value,
        caracteristicas: f.controls.caracteristicas.value,
        tipo_producto: f.controls.tipo_producto.value
      }

    ).then(
      (t: any) =>  {
        console.log('Datos guardados!: ' + t.key);

      },
      (e: any) => console.log(e.message));
   /* f.controls.codigo.setValue('');
    f.controls.nombre.setValue('');
    f.controls.descripcion.setValue('');
    f.controls.percio.setValue('');
    f.controls.foto.setValue('');
    f.controls.tipo.setValue('');
    f.controls.stock.setValue('');
    // f.controls.color.setValue('');
    f.controls.dimensiones.setValue('');
    f.controls.peso.setValue('');
    f.controls.caracteristicas.setValue('');
    f.controls.tipo_producto.setValue('');
*/
  }


  detectFiles(event) {
    this.selectedFiles = event.target.files;
    if (this.selectedFiles.length > 5) {
      alert('Solo puede subir hasta 5 fotos');
      // event.target.files= new FileList();
     // event.selectedFiles= new FileList();
    }

  }



}

