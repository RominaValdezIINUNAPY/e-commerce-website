import { Component, OnInit } from '@angular/core';
import {ProductosService} from '../../services/productos.service';
import {VentasService} from '../../services/ventas.service';

@Component({
  selector: 'app-graficos',
  templateUrl: './graficos.component.html',
  styleUrls: ['./graficos.component.css']
})
export class GraficosComponent implements OnInit {
  desde: string;
  hasta: string;

  grafico_1 = {};
  grafico_2 = {};
  grafico_3 = {};
  grafico_4 = {};


  constructor(private  productosService: ProductosService, private  ventasService: VentasService) {
    setTimeout(() => this.redibujar(), 800);
  }

  ngOnInit() {
  }

  redibujar() {
    console.log('Gráficos');
    console.log('Desde= ' + this.desde);
    console.log('Hasta= ' + this.hasta);
    this.ventasService.ventasFiltrado(this.desde, this.hasta);
    this.dibujarGrafico1();
    this.dibujarGrafico2();
    this.dibujarGrafico3();
    this.dibujarGrafico4();
  }

  private dibujarGrafico1() {
    const nombre_prods = [];
    const valores_prods = [];

    for (const producto of this.productosService.productos){
      nombre_prods.push(producto.cod);
      valores_prods.push({value: this.ventasService.ventasProductoMes(producto.cod, undefined) , name: producto.cod});
    }

    this.grafico_1 = {
      title : {
        text: 'Gráfico de Torta',
        subtext: 'Productos',
        x: 'center'
      },
      tooltip : {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      legend: {
        orient: 'vertical',
        left: 'left',
        data: nombre_prods
      },
      series : [
        {
          name: 'Productos',
          type: 'pie',
          radius : '55%',
          center: ['50%', '60%'],
          data: valores_prods,
          itemStyle: {
            emphasis: {
              shadowBlur: 10,
              shadowOffsetX: 0,
              shadowColor: 'rgba(0, 0, 0, 0.5)'
            }
          }
        }
      ]
    };
  }

  private dibujarGrafico2() {
    const nombre_prods = [];
    const valores_prods = [];

    for (const producto of this.productosService.productos){
      nombre_prods.push(producto.cod);
      const datos = [];
      let cant = 0;
      for (let i = 0; i < 12; i++) {
        cant += this.ventasService.ventasProductoMes(producto.cod, i)
        datos.push(cant);
      }
      valores_prods.push(
        {
          name: producto.cod,
          type: 'line',
          data: datos
        });
    }

    this.grafico_2 = {
      title: {
        text: '\n\nGráfico de líneas "acumuladas"\n\n',
        left: 'center'
      },
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c}'
      },
      legend: {
        left: 'left',
        data: nombre_prods
      },
      xAxis: {
        type: 'category',
        name: 'x',
        splitLine: {show: false},
        data: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre']
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      yAxis: {
        type: 'log',
        name: 'y'
      },
      series: valores_prods
    };

  }

  private dibujarGrafico3() {
    const nombre_prods = [];
    const valores_prods = [];

    for (const producto of this.productosService.productos){
      nombre_prods.push(producto.cod);
      const datos = [];
      for (let i = 0; i < 12; i++) {
        datos.push(this.ventasService.ventasProductoMes(producto.cod, i));
      }
      valores_prods.push(
        {
          name: producto.cod,
          type: 'bar',
          data: datos
        });
    }

    this.grafico_3 = {
      title : {
        text: '\n\nGráfico de barras múltiples\n\n\n\n\n\n',
        subtext: 'Ventas'
      },
      tooltip : {
        trigger: 'axis'
      },
      legend: {
        data: nombre_prods
      },
      toolbox: {
        show : false,
      },
      calculable : true,
      xAxis : [
        {
          type : 'category',
          data : ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre']
        }
      ],
      yAxis : [
        {
          type : 'value'
        }
      ],
      series : valores_prods
    };

  }

  private dibujarGrafico4(){
    const nombre_prods = [];
    const valores_prods = [];

    for (const producto of this.productosService.productos){
      nombre_prods.push(producto.cod);
      const datos = [];
      for (let i = 0; i < 12; i++) {
        datos.push(this.ventasService.ventasProductoMes(producto.cod, i));
      }
      valores_prods.push(
        {
          name: producto.cod,
          type: 'bar',
          stack: '总量',
          label: {
            normal: {
              show: true,
              position: 'insideRight'
            }
          },
          data: datos
        });
    }
    this.grafico_4 = {
      title : {
        text: '\n\nGráfico de barras apiladas\n\n',
        subtext: 'Ventas'
      },
      tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
          type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      legend: {
        data: nombre_prods
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      xAxis:  {
        type: 'value'
      },
      yAxis: {
        type: 'category',
        data: ['Enero  ', 'Febrero  ', 'Marzo  ', 'Abril  ', 'Mayo  ', 'Junio  ', 'Julio  ', 'Agosto  ', 'Setiembre  ', 'Octubre  ', 'Noviembre  ', 'Diciembre  ']
      },
      series: valores_prods
    };

  }
}
