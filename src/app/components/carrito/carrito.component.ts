import {Component, OnInit} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import { NgForm } from '@angular/forms';
import {Upload} from '../../services/upload';
import {UploadService} from '../../services/upload.service';
import * as _ from 'lodash';
import {CarritoService} from '../../services/carrito.service';
import {ProductosService} from '../../services/productos.service';
import {VentasService} from "../../services/ventas.service";

@Component({
  selector: 'app-carrito',
  templateUrl: 'carrito.component.html',
  styleUrls: ['carrito.component.css']
})


export class CarritoComponent implements OnInit {
  selectedValue: string;
  selectedFiles: FileList;
  currentUpload: Upload;
  color: string;





  ngOnInit() {
  }

  constructor(
    private angularFire: AngularFireDatabase,
    private upSvc: UploadService,
    public carritoService: CarritoService,
    public productoService: ProductosService,
    public ventasService: VentasService
  ) { }

  form_submit(f: NgForm) {
    const files = this.selectedFiles;

    if (_.isEmpty(files)) { return ; }

    if (files.length > 5) { return alert('Solo puede subir hasta 5 fotos'); }

    const filesIndex = _.range(files.length);
    _.each(filesIndex, (idx) => {
      this.currentUpload = new Upload(files[idx]);
      this.upSvc.pushUpload(this.currentUpload, f.controls.codigo.value); }
    );


    this.angularFire.list('compra').push(
      {

        producto: f.controls.producto.value,
        costo: f.controls.costo.value,
        cantidad: f.controls.cantidad.value

      }

    ).then(
      (t: any) =>  {
        console.log('Datos guardados!: ' + t.key);

      },
      (e: any) => console.log(e.message));

  }


  detectFiles(event) {
    this.selectedFiles = event.target.files;
    if (this.selectedFiles.length > 5) {
      alert('Solo puede subir hasta 5 fotos');
      // event.target.files= new FileList();
      // event.selectedFiles= new FileList();
    }

  }
}

