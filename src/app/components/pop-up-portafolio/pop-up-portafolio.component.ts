import { Component, OnInit } from '@angular/core';

import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import {Producto} from '../../models/producto';
import {CarritoService} from '../../services/carrito.service';

export interface ConfirmModel {
  title: string;
  message: string;
  producto: Producto;
}

@Component({
  selector: 'pop-up-portafolio',
  templateUrl: './pop-up-portafolio.component.html',
  styleUrls: [
    './pop-up-portafolio.component.css'
  ]
})
export class PopUpPortafolioComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel {
  title: string;
  message: string;
  producto: Producto;
  cod: string;
  selectedIndex: number;
  cantidad: number;

  constructor(dialogService: DialogService, private  carritoService: CarritoService) {
    super(dialogService);
  }
  confirm() {
    // we set dialog result as true on click on confirm button,
    // then we can get dialog result from caller code
    this.carritoService.agregarProducto(this.producto, this.cantidad);
    this.result = true;
    this.close();
  }

  select(index: number) {
    this.selectedIndex = index;
    this.producto.foto = this.producto.fotos[index];
  }

}
