import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopUpPortafolioComponent } from './pop-up-portafolio.component';

describe('PopUpPortafolioComponent', () => {
  let component: PopUpPortafolioComponent;
  let fixture: ComponentFixture<PopUpPortafolioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopUpPortafolioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopUpPortafolioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
