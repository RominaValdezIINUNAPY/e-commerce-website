import { Component } from '@angular/core';
import { InformacionService } from "../../services/informacion.service";
import { Router } from "@angular/router";
import {AuthService} from "../../modules/auth";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: [
    '../../../assets/css/aurora-pack.min.css',
    '../../../assets/css/aurora-theme-base.min.css',
    '../../../assets/css/urku.css'
  ]
})
export class HeaderComponent {
  subscription: Subscription;
  authenticated: boolean;
  auth:AuthService;
  cantCarrito = 0;
  precioCarrito = 0;

  constructor(
    public _is:InformacionService,
    private router:Router,
    private authservice: AuthService
  ){
    this.auth = authservice;
    this.subscription = authservice.authenticated$.subscribe(
      b => this.authenticated = b
  );
  }

  buscar_producto( termino:string){

    // console.log(termino);
    this.router.navigate( ['buscar', termino ] );

  }

  public getCantCarrito(){
    return this.cantCarrito;
  }

  public getPrecioCarrito(){
      return this.cantCarrito;
  }

  public setPrecioCarrito(n: number){
    this.cantCarrito = n;
  }

  public setCantCarrito(n: number){
    this.cantCarrito = n;
  }
}



