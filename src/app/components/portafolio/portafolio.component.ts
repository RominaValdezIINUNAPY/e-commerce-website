import { Component } from '@angular/core';
import { ProductosService } from '../../services/productos.service';
import { DialogService } from 'ng2-bootstrap-modal';
import {PopUpPortafolioComponent} from '../pop-up-portafolio/pop-up-portafolio.component';
import {Producto} from "../../models/producto";

@Component({
  selector: 'app-portafolio',
  templateUrl: './portafolio.component.html',
  styleUrls: [
    '../../../assets/css/aurora-pack.min.css',
    '../../../assets/css/aurora-theme-base.min.css',
    '../../../assets/css/urku.css'
  ]
})
export class PortafolioComponent {

  constructor(
    public _ps: ProductosService,
    private dialogService: DialogService
  ) {}

  agregarAlCarrito(prod: Producto) {
    const disposable = this.dialogService.addDialog(PopUpPortafolioComponent, {
      title: 'Agregar al carrito',
      message: 'Desea agregar al carrito?',
      producto: prod
    })
      .subscribe((isConfirmed) => {
        // We get dialog result
        if (isConfirmed) {
           alert(prod.nombre + ' agregado al carrito');
        }
        else {
          //alert('declined');
        }
      });
    // We can close dialog calling disposable.unsubscribe();
    // If dialog was not closed manually close it by timeout
    // setTimeout(()=>{
    //  disposable.unsubscribe();
    //} ,10000);
  }
}
