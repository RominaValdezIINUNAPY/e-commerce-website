import { Component } from '@angular/core';
import { InformacionService } from "../../services/informacion.service";

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: [
    '../../../assets/css/aurora-pack.min.css',
    '../../../assets/css/aurora-theme-base.min.css',
    '../../../assets/css/urku.css'
  ]
})
export class AboutComponent {

  constructor( public _is: InformacionService ) { }

}
