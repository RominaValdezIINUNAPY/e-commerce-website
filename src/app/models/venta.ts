export class Venta {
  cabecera: Cabecera;
  detalles: DetalleVenta[];

  constructor(detalles: DetalleVenta[], monto: number, fecha: any) {
    this.cabecera = new Cabecera();
    this.cabecera.fecha = fecha;
    this.cabecera.monto = monto;
    this.detalles = detalles;
  }
}

class Cabecera {
  monto: number;
  fecha: any;

}

export class DetalleVenta {
  producto_cod: string;
  cantidad: number;
  constructor(cod: string, cantidad: number) {
    this.producto_cod = cod;
    this.cantidad = cantidad;
  }
}
