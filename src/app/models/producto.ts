
export class Producto {

  categoria: string;
  cod: string;
  nombre: string;
  foto: string;
  precio: number;
  fotos: string[];
  color: string;
  dimensiones: string;
  peso: string;
  caracteristicas: string;
  constructor() {

  }
}

