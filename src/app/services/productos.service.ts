import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Producto} from '../models/producto';
import {Observable} from 'rxjs/Observable';
import {AngularFireDatabase} from 'angularfire2/database';


@Injectable()
export class ProductosService {

  productos: Producto[]= [];
  productos_filtrado: Producto[] = [];
  cargando = true;

  constructor( private http: Http, private db: AngularFireDatabase ) {
    this.cargar_productos();
  }

  public buscar_producto( termino: string ) {

    // console.log("Buscando producto");
    // console.log( this.productos.length );


    if ( this.productos.length === 0 ) {
      this.cargar_productos().then( () => {
        // termino la carga
        this.filtrar_productos(termino);
      });
    }else {
      this.filtrar_productos(termino);
    }

  }

  private filtrar_productos(termino: string) {

    this.productos_filtrado = [];

    termino = termino.toLowerCase();

    this.productos.forEach( prod => {

      if ( prod.categoria.indexOf( termino ) >= 0 || prod.nombre.toLowerCase().indexOf( termino ) >= 0 ) {
        this.productos_filtrado.push( prod );
      }
    });

  }


  public cargar_producto( cod: string ) {
    return this.http.get(`https://webangular4.firebaseio.com/Productos/${ cod }.json`);
  }


  public cargar_productos() {
    if (this.productos.length === 0) {
      this.cargando = true;

      const promesa = new Promise((resolve, reject) => {

        this.http.get('https://segundo-parcial-c6a99.firebaseio.com/productos.json')
          .subscribe(res => {
            this.cargando = false;
            this.productos = res.json();
            resolve();
          });
      });

      return promesa;
    }
  }

  public get_producto( cod: string ) {
    for (const prod of this.productos){
      if (prod.cod === cod) {
        return prod;
      }
    }
    return undefined;
  }


}
