import { Injectable } from '@angular/core';
import {AngularFireDatabase, FirebaseListObservable} from 'angularfire2/database';
import { Venta} from '../models/venta';
import {CarritoService} from './carrito.service';
import {Observable} from 'rxjs/Observable';
import {Http} from '@angular/http';

@Injectable()
export class VentasService {
  ventas: Venta[] = [];
  ventas_filtrado: Venta[]= [];

  nueva_venta: Venta;

  constructor(private angularFire: AngularFireDatabase,  private carritoService: CarritoService, private http: Http) {
    this.cargar_ventas();
  }
  public guardarVenta() {
    const fecha = new Date().toISOString();
    this.nueva_venta = new Venta(this.carritoService.detalles, this.carritoService.total, fecha);
    const idx = this.ventas.length;
    const itemRef = this.angularFire.object('ventas/' + idx);
    const promesa = itemRef.set(this.nueva_venta);
    promesa.then(
      _ => {
        this.ventas.push(this.nueva_venta);
        console.log('Venta guardada!');
          alert('Compra guardada!');
          this.carritoService.borrarCarrito();
      });
    promesa.catch(
      err => {
        console.log(err, 'No se pudo guardar la compra');
        alert('No se pudo guardar la compra');
      }
    );

  }

  public cargar_ventas() {
    if (this.ventas.length === 0) {
      const promesa = new Promise((resolve, reject) => {

        this.http.get('https://segundo-parcial-c6a99.firebaseio.com/ventas.json')
          .subscribe(res => {
            this.ventas = res.json();
            this.ventas_filtrado = res.json();
            resolve();
            // this.ventasFiltrado('2017-11-01T22:51:00.000Z', '2017-11-01T23:27:00.000Z');

          });
      });

      return promesa;
    }

  }

  public ventasFiltrado(inicio: string, fin: string) {
     console.log('Filtrando');
    // this.cargar_ventas();
    this.ventas_filtrado = [];
    for (const venta of this.ventas) {
      const fecha_venta = new Date(Date.parse(venta.cabecera.fecha)).getTime();
      let fecha_inicio: number;
      let fecha_fin: number;
      if (inicio !== '' && inicio !== undefined) {
        fecha_inicio = new Date(Date.parse(inicio)).getTime();

      }else {
        fecha_inicio = undefined;
      }
      if (fin !== '' && fin !== undefined) {
        fecha_fin = new Date(Date.parse(fin)).getTime() + 86399;
      }else {
        fecha_fin = undefined;
      }

      if ((fecha_inicio === undefined || fecha_inicio <= fecha_venta ) &&  (fecha_fin === undefined || fecha_venta <= fecha_fin )) {
        this.ventas_filtrado.push(venta);
        // console.log( new Date(Date.parse(venta.cabecera.fecha)).toLocaleTimeString());
      }
    }
  }

  public ventasProductoMes(cod_producto: string, mes: number) {
    let cant: number;
    cant = 0;
    for (const venta of this.ventas_filtrado){
      const mes_venta = new Date(Date.parse(venta.cabecera.fecha)).getMonth();
      if (mes_venta === mes || mes === undefined) {
        for (const detalle of venta.detalles) {
          if (detalle.producto_cod === cod_producto || cod_producto === undefined) {
            cant += detalle.cantidad;
          }
        }
      }
    }
    return cant;
  }

}


