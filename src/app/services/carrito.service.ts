import { Injectable } from '@angular/core';
import {Producto} from '../models/producto';
import {DetalleVenta} from '../models/venta';

@Injectable()
export class CarritoService {
  detalles: DetalleVenta[] = [];
  items: number;
  total: number;
  hayProductos = false;
  constructor() {
    this.items = 0;
    this.total = 0;
  }

  public agregarProducto(producto: Producto, cantidad: number) {
    const nuevo = new DetalleVenta(producto.cod, cantidad);
    this.detalles.push(nuevo);
    this.items += cantidad;
    this.total += producto.precio * cantidad;
    this.hayProductos = true;
  }

  public borrarCarrito() {
    this.detalles = [];
    this.items = 0;
    this.total = 0;
    this.hayProductos = false;
  }
}
